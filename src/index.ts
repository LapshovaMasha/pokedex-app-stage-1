const root = document.getElementById('root')

if (!root) {
    throw new Error('root not found')
}

root.innerHTML = "<H1>Hello Pokemon!</H1>"
